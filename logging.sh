#!/bin/bash


# Последний файл в каталоге /tmp/test/files, который был изменен за последние 7 дней.
file=$(find /tmp/test/files/ -type f -mtime -7 -printf '%T+ %p\n' | sort | tail -1 | cut -f2- -d" ")

# Если файл был найден
if [ -n "$file" ]; then
    # Имя файла и объем
    name=$(basename "$file")
    volume=$(du -sh "$file" | awk '{print $1}')
    # Запись информации в log файл
    echo "$(date): $file $name $volume" >> /tmp/test/script-log/sync.log
    # Сжатие файла в архив .tar.gz
    tar -czvf "/tmp/test/archives/${name}_$(date +%Y-%m-%d_%H-%M-%S).tar.gz" "$file"
    # Переместить архив в папку /tmp/test/archives
    if mv "/tmp/test/archives/${name}_$(date +%Y-%m-%d_%H-%M-%S).tar.gz" /tmp/test/archives/; then
        # Если перемещение прошло успешно исходный файл удалится
        rm "$file"
    fi
fi
